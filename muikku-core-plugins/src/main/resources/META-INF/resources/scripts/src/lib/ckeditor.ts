function getCKEDITOR(){
  return (<any>window).CKEDITOR;
}

const CKEDITOR_VERSION = "4.11.1";

export {CKEDITOR_VERSION};
export default getCKEDITOR;